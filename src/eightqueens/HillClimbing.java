/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eightqueens;

import java.util.ArrayList;
import javax.swing.JLabel;

public class HillClimbing {

    private Tabuleiro initial;
    public HillClimbing(Tabuleiro initial) {
        this.initial = initial;
    }
    
    public String solve(JLabel label, JLabel label1){
        if(initial!=null){
            Tabuleiro Tcurrent = initial;
            Tabuleiro Tnext = Tcurrent.generateBestMove();
            ComparaTabuleirosH ct = new ComparaTabuleirosH();
            int cont = 0;
            int ntest = 1;
            cont = ct.compare(Tnext,Tcurrent);
            int i = 1 ;
            String Scurrent = Tcurrent.toString();
            WriteTxt wt = new WriteTxt(1);
            ArrayList<String> results = new ArrayList<String>();
            results.add(Scurrent);
            if (Tcurrent.getH() > 0 || cont != 0){
            	while (cont == -1){
	            	Tcurrent = Tnext;		            	
	            	Tcurrent.setG(i++);
            		Tnext = Tcurrent.generateBestMove();
	            	cont = ct.compare(Tnext,Tcurrent);
	            	Scurrent = Tcurrent.toString();
	            	results.add(Scurrent);
                        ntest++;
	            	         	
	            }
            }
            else Tcurrent.setG(0);
            //Scurrent = Tcurrent.toString();
            //results.add(Scurrent);
            //wt.writeArray(results);
            label1.setText(String.valueOf(Tcurrent.getG()));
            label.setText(String.valueOf(ntest));
            return wt.writeArray(results);
        }
		return null;    
    }

}