/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eightqueens;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Stack;
import java.util.TreeSet;
import javax.swing.JLabel;

/**
 *
 * @author henrique
 */
public class AStar {

    private Tabuleiro initial;
    private TreeSet<Tabuleiro> openSet;
    private HashMap<String,Tabuleiro> openMap;
    private TreeSet<String> closedSet;
    private HashMap<String,String> cameFrom;   // atual, pai. atual = key, pai = value
    public AStar(Tabuleiro initial) {
        this.initial = initial;
    }
    
    public String solve(JLabel label, JLabel label1){
        if(initial!=null){
            Tabuleiro Tcurrent = initial;
            String Scurrent = initial.toString();
            ComparaTabuleiros ct = new ComparaTabuleiros();
            Tcurrent.setG(0); // profundidade (custo g) do primeiro estado é zero
            Tcurrent.setF();
            openMap = new HashMap<String,Tabuleiro>();
            openMap.put(Scurrent, Tcurrent);
            openSet = new TreeSet<Tabuleiro>(ct);  //cria set dos estados abertos com comparador pela heuristica, se for 0 fica sempre na frente, ou pelo custo f caso h !=0
            openSet.add(Tcurrent);
            closedSet = new TreeSet<String>();
            cameFrom = new HashMap<String,String>();
            cameFrom.put(Scurrent, null);
            int ntest = 1;
            while(!openSet.isEmpty()){
                Tcurrent = openSet.pollFirst();     //tira primeiro estado do set
                Scurrent = Tcurrent.toString();
                openMap.remove(Scurrent);    //remove ele do mapa tbm
                closedSet.add(Scurrent);    //adiciona referencia (string) do estado removido nos fechados
                //ntest++;
                if(Tcurrent.getH()==0){
                    //System.out.print("\nresolveu "+ Scurrent+ "prof: "+ Tcurrent.getG()+"\n");
                    openSet.clear();
                    closedSet.clear();
                    openMap.clear();
                    label1.setText(String.valueOf(Tcurrent.getG()));
                    label.setText(String.valueOf(ntest));
                    WriteTxt wt = new WriteTxt(0);
                    return wt.writeStack(reconstruct_path(Scurrent));
                }
                ArrayList<Tabuleiro> neighbors = Tcurrent.generateTopNNeighbors(10);  // gera os n melhores neighbors, ja com suas heuristicas calculadas
                for(int i = 0; i < neighbors.size(); i++){
                    Tabuleiro actual_neighbor = neighbors.get(i);
                    if(closedSet.contains(actual_neighbor.toString())){  // se um dos vizinhos ja estiver no set fechados, pula o ciclo e dispensa ele
                        continue;
                    }
                    ntest++;
                    actual_neighbor.setG(Tcurrent.getG()+1);  //prof do estado gerador +1
                    actual_neighbor.setF(); //calcula F, somando G e H para actual neighbor
                    if(!openMap.containsKey(actual_neighbor.toString())){   //se nao existe gemeo do actual_neighbor (comparaçao dada pela string referencia), só adicionar ele no set e no mapa
                        openSet.add(actual_neighbor);  //o set é auto-ordenado, ao adicionar algo, automaticamente ordena o set inteiro
                        openMap.put(actual_neighbor.toString(), actual_neighbor);
                        cameFrom.put(actual_neighbor.toString(), Scurrent);
                    }
                    else{        //estado ja esta no mapa e no set, pendente pra ser analisado, verifica se o estado gerado agora tem melhor custo que o seu gemeo antigo
                        Tabuleiro gemeo_an = openMap.get(actual_neighbor.toString());  //string de referencias deles é a mesma, obviamente, pq sao gemeos
                        if(gemeo_an.getF() > actual_neighbor.getF() ){  //faz a troca pq o vizinho encontrado agora é melhor
                            openSet.remove(gemeo_an);
                            openSet.add(actual_neighbor);
                            openMap.remove(actual_neighbor.toString());  // poderia ser gemeo.an.toString(), mas tanto faz
                            openMap.put(actual_neighbor.toString(), actual_neighbor);
                            cameFrom.remove(actual_neighbor.toString());
                            cameFrom.put(actual_neighbor.toString(), Scurrent);
                        }//61225713
                    }
                
                }
                
            }
        }
        return null;
    }

    private Stack<String> reconstruct_path(String Scurrent) {
        Stack<String> path = new Stack<String>();
        String current = Scurrent;
        do{
            path.push(current);
            //System.out.print("\nempilhou "+current);
            current = cameFrom.get(current);
        }while(current!=null);
        return path;
    }

    
    
}
