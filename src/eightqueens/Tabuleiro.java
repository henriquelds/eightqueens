/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eightqueens;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;

/**
 *
 * @author henrique
 */
public class Tabuleiro {
    private ArrayList<Integer> tab = new ArrayList<>(8);     //estado do tabuleiro, onde a posiçao no array indica a coluna (sempre tem uma rainha por coluna)
    private int h;      //heuristica                         // e o valor da posiçao indica em que linha a rainha esta nessa coluna
    private int g;      //custo = profundidade para A*, para os outros nao é usado
    private int f; //g+h
    public Tabuleiro(ArrayList<Integer> tab) {
        this.tab = tab;
        
    }

    public ArrayList<Integer> getTab() {
        return tab;
    }

    public void setTab(ArrayList<Integer> tab) {
        this.tab = tab;
    }
    
    
    public int getH() {
        return h;
    }

    public void setH(int h) {
        this.h = h;
    }

    public int getG() {
        return g;
    }

    public void setG(int g) {
        this.g = g;
    }

    public int getF() {
        return f;
    }

    public void setF(int f) {
        this.f = f;
    }
    
    public void setF(){
        this.f = this.g + this.h;
    }
    
    
    
    public int calculateHeuristic(){ //numero de pares de rainhas que se atacam, MINGLOBAL=0
        int h=0;
        int linha_vizinho;
        int linha_atual;
        for(int coluna_atual=0; coluna_atual <=6; coluna_atual++){
            linha_atual = tab.get(coluna_atual);
            for(int coluna_vizinho = coluna_atual+1; coluna_vizinho <= 7; coluna_vizinho++){      //quem se ataca com a rainha da coluna 0
                linha_vizinho = tab.get(coluna_vizinho);
                if(linha_vizinho==linha_atual){      //verifica ataques na linha
                    h++;
                }
                else{
                    int diag_cima_linha = linha_atual - coluna_vizinho + coluna_atual;   //verifica ataques na diagonal superior
                    if(linha_vizinho==diag_cima_linha){
                        h++;
                    }
                    else{
                        int diag_baixo_linha = linha_atual + coluna_vizinho - coluna_atual;  //verifica ataques na diagonal inferior
                        if(linha_vizinho==diag_baixo_linha){
                            h++;
                        }
                    }
                }
            }
        }
    setH(h);    
    return h;
}

    public Tabuleiro takeAleatTab(){
      	Random gerador = new Random();
      	      	
        int cont1,cont2;
        Tabuleiro novo;
        ArrayList<Integer> novo_tab;
        ArrayList<Tabuleiro> neighbors = new ArrayList<Tabuleiro>();
        for(cont1 = 0; cont1 <= 7; cont1++){   //gera os 56 vizinhos
            for(cont2 = 0; cont2 <= 7; cont2++){
                if(this.getTab().get(cont1) != cont2){
                    novo_tab = new ArrayList<Integer>(this.getTab());
                    novo_tab.set(cont1, cont2);
                    novo = new Tabuleiro(novo_tab);
                    novo.calculateHeuristic();
                    neighbors.add(novo);
                }
            }
        }
        ComparaTabuleirosH cth = new ComparaTabuleirosH();
        Collections.sort(neighbors, cth);  //ordena somente pela heuristica
       
        return neighbors.get(gerador.nextInt( neighbors.size()));
    }
    public ArrayList<Tabuleiro> generateTopNNeighbors(int n) {
        int cont1,cont2;
        Tabuleiro novo;
        ArrayList<Integer> novo_tab;
        ArrayList<Tabuleiro> neighbors = new ArrayList<Tabuleiro>();
        for(cont1 = 0; cont1 <= 7; cont1++){   //gera os 56 vizinhos
            for(cont2 = 0; cont2 <= 7; cont2++){
                if(this.getTab().get(cont1) != cont2){
                    novo_tab = new ArrayList<Integer>(this.getTab());
                    novo_tab.set(cont1, cont2);
                    novo = new Tabuleiro(novo_tab);
                    novo.calculateHeuristic();
                    neighbors.add(novo);
                }
            }
        }
        ComparaTabuleirosH cth = new ComparaTabuleirosH();
        Collections.sort(neighbors, cth);  //ordena somente pela heuristica, pq ainda nao se sabe o custo G de cada um
        neighbors = new ArrayList<Tabuleiro>(neighbors.subList(0, n));  //pega os n melhores
        return neighbors;
    }
    
    public Tabuleiro generateBestMove() {
        int cont1,cont2;
        Tabuleiro novo;
        ArrayList<Integer> novo_tab;
        ArrayList<Tabuleiro> neighbors = new ArrayList<Tabuleiro>();
        for(cont1 = 0; cont1 <= 7; cont1++){   //gera os 56 vizinhos
            for(cont2 = 0; cont2 <= 7; cont2++){
                if(this.getTab().get(cont1) != cont2){
                    novo_tab = new ArrayList<Integer>(this.getTab());
                    novo_tab.set(cont1, cont2);
                    novo = new Tabuleiro(novo_tab);
                    novo.calculateHeuristic();
                    neighbors.add(novo);
                }
            }
        }
        ComparaTabuleirosH cth = new ComparaTabuleirosH();
        Collections.sort(neighbors, cth);  //ordena somente pela heuristica
        return neighbors.get(0);
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        for(int i = 0; i < tab.size(); i++){
            sb.append(tab.get(i));
        }
        return sb.toString();
    }
    
}
