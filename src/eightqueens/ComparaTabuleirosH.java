/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eightqueens;

import java.util.Comparator;

/**
 *
 * @author henrique
 */
public class ComparaTabuleirosH implements Comparator<Tabuleiro> {

    @Override
    public int compare(Tabuleiro t, Tabuleiro t1) {
        if((t.getH() < t1.getH())){
            return -1;
        }
        else if(t.getH() > t1.getH()){
            return 1;
        }
        else{
            return 0;
        }
    }
    
}
