/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eightqueens;

import java.util.ArrayList;
import javax.swing.JButton;

/**
 *
 * @author henrique
 */
public class EightQueensProg {

    /**
     * @param args the command line arguments
     */
    private static int[] vet = new int[] {6,1,2,2,5,7,1,3};//{0,1,2,3,4,5,6,7};
    public static void main(String[] args) {
        ArrayList<Integer> initial = new ArrayList<Integer>(8);
        completaArray(initial,vet);
        Tabuleiro table = new Tabuleiro(initial);
        //System.out.print("h= "+table.calculateHeuristic());
        Frame fr = new Frame(table);
        fr.setVisible(true);
    }

    private static void completaArray(ArrayList<Integer> initial, int[] vet) {
        for(int i=0; i < 8; i++){
            initial.add(vet[i]);
        }
        
    }
    
}
