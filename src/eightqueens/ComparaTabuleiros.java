/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eightqueens;

import java.util.Comparator;

/**
 *
 * @author henrique
 */
public class ComparaTabuleiros implements Comparator<Tabuleiro> {

    @Override
    public int compare(Tabuleiro t, Tabuleiro t1) {
      if(t.getH() == 0){
          return -1;
      }
      else{
        if(t1.getH() == 0){
            return 1;
        }  
        else{
            if((t.getF() < t1.getF())){
                return -1;
            }
            else if(t.getF() > t1.getF()){
                return 1;
            }
            else{
                return 0;
            }
        }
    }
    }
}
