/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eightqueens;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Random;
import java.util.Stack;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author henrique
 */
public class WriteTxt {
    private BufferedWriter bw;
    private String path;
    
    public WriteTxt(int i) {
    Random rd = new Random();
    if(i == 0){  // A*
        this.path = "Astar-results"+rd.nextInt(1000)+".txt";
        
    }
    else if(i == 1){ // hill climbing
        this.path = "HillC-results"+rd.nextInt(1000)+".txt";
    }
    else if(i == 2){ // tempera simulada
        this.path = "TempSim-results"+rd.nextInt(1000)+".txt";
    }
    }
    
    public String writeStack(Stack<String> results){
        try {
            bw = new BufferedWriter(new FileWriter(path));
            while(!results.isEmpty()){
                bw.write(results.pop()+"\n");
                bw.flush();
            }
        
        
        } catch (IOException ex) {
            Logger.getLogger(WriteTxt.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return path;
    }
    
    public String writeArray(ArrayList<String> results){
        try {
            bw = new BufferedWriter(new FileWriter(path));
            for(int i = 0; i < results.size(); i++){
                bw.write(results.get(i)+"\n");
                bw.flush();
            }
        
        
        } catch (IOException ex) {
            Logger.getLogger(WriteTxt.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return path;
    }
    
    
    
}
