package eightqueens;
import java.lang.Math;
import java.util.ArrayList;
import java.util.Random;
import java.util.Stack;
import javax.swing.JLabel;

public class TemperaSimulada {
	
    private Tabuleiro initial;
    public TemperaSimulada(Tabuleiro initial) {
        this.initial = initial;
    }
    
    public String solve(JLabel label, JLabel label1){
        if(initial!=null){
        	Random gerador = new Random();
        	double temperatura = gerador.nextInt(1000)+100; // valor entre 1000 e 1200
        	double passo=0.9645; // quanto mais proximo de 99 melhor � o resultado
            
        	Tabuleiro Tcurrent = initial;
        	String Scurrent = Tcurrent.toString();
        	
            Tabuleiro Tnext ;
          
            int dE ;
            
          
            ArrayList<String> path= new ArrayList<String>();
            path.add(Scurrent);
          
           int cont=0, i=0;
            while(temperatura>0.00002 ){
            	
            	i++;
            	temperatura=temperatura*passo;

        		Tnext = Tcurrent.takeAleatTab();
        		dE = Tnext.calculateHeuristic() - Tcurrent.calculateHeuristic();
    		    double aux = dE/temperatura;
                double probabili= Math.pow(Math.E, -aux) ;

        		if(dE < 0 ){
        			cont++;
        			Tcurrent = Tnext;
        			Scurrent = Tcurrent.toString();
        			path.add(Scurrent);
        		}
        		else{        			
        			if( Math.random() <= probabili) {
        				//System.out.println("probabili_ "+probabili);
        				cont++;
        				Tcurrent = Tnext;
        				Scurrent = Tcurrent.toString();
            			path.add(Scurrent);
        			}
        		}
        		
        		
        		Scurrent = Tcurrent.toString();
                //System.out.println("\n" + Tcurrent.calculateHeuristic() + " colisoes |"
                //		+ " profundidade:" + cont + " | itera��es: " +i+"\n");
               
                if(Tcurrent.calculateHeuristic()==0 )
                	break;
            }
            label1.setText(String.valueOf(cont));
            label.setText(String.valueOf(i));
            WriteTxt wt = new WriteTxt(2);
           
            return  wt.writeArray(path);
        }
      
        return null;
    }
}
